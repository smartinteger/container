<?php

use Mvccontainer\Core\Test;
use Mvccontainer\core\Routing\Router;
use Mvccontainer\core\Logging\{Logger,Format,FileWriter,LoggerFactory};
use  Mvccontainer\core\Database\DbFactory;
return [
    // Классы которые будут загружены при запуске приложения
    'Test'=> Test::class,
    'service' => [
       

        'Db' =>[
            'class'=> DbFactory::class,
            'depend'=> [
             'connect'=>[
                  'host' => 'localhost',
                  'user' => 'root',
                  'pass' => 'root',
                  'db'=>'itea'
                ],
             'sql'=>'mysql'
            ], 
        
         ],
        
    ],
    // Алиасы для классов
    'component'  => [       
        'Route' => Router::class, 
        'Test'=> Test::class, 
        'logger' =>LoggerFactory::class,
      
       // 'Logger' =>Logger::class,      
    ],

    'file'  => [
        'routes' => $_SERVER['DOCUMENT_ROOT'] . '/src/app/routes/web.php',        
    ]
 

];