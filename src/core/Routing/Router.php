<?php
 
namespace Mvccontainer\core\Routing;
use Mvccontainer\core\Contracts\BootstrapInterface;
use Mvccontainer\App\Controllers\IndexController;
use Mvccontainer\core\Routing\ParseUrl;


class Router implements BootstrapInterface, RouterInterface
{
    
 use ParseUrl;
  
    protected $file;

    /**
     * @var array
     */
    protected static $patterns = [
        '{integer}' => '[0-9]+',
        '{string}'  => '[a-zA-Z]+',
        '{any}'     => '/\/(\d+)$/',
    ];

    public function __construct($file)
    {
        $this->file = $file;
        $this->bootstrap();
    }
 /*
   *
     * Служит главной точкой старта для системы роутеров
     * Метод проверяет на роутеры на совпадение
     */

    public function route()
    {
      
       //TODO
    }


    public function bootstrap()
    {
        include $this->file;
    }

    /**
     * Принимаем аргументы из файла роутинга
     * @param array ...$arguments
     */
    public static function get(...$arguments): void
    {
        static::addRouter('GET', $arguments);

    }
 
    /**
     * метод непосредственно вызывает пользовательские методы
     * @return void
     */

    private static function initRout($arguments, $arg_function=null): void
    { 
        
       if (is_callable($arguments)) {//принадлежит ли переданное ей значение псевдотипу callable
          call_user_func($arguments);
       } else { 
           $controller = explode('@',$arguments);
           $_class = '\Mvccontainer\App\Controllers\\'.$controller[0];
           call_user_func([new $_class, $controller[1].'Action'], $arg_function);
         
       } 
    }

       /**
     * @param string $method
     * @param        $arguments
     */
    private static function addRouter(string $method, $arguments): void
    {
      
        $currentPath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        $argumentMethod = self::replaceUrl($currentPath);

        $arg = self::parse($arguments[0]);
        $arg_path = self::parse($currentPath);     
       
      
        if($arg[3]!=null && $arg_path[3]!=null){//если есть вторая часть /index/{integer} 
           // извлекаем последнюю часть имени хоста со слешем

           preg_match('/\/(\d+)$/', $currentPath, $matches);
        
           
           if ('/'.$arg[1].'/'.$arg[2].$matches[0]==$argumentMethod) {
               //формируем сравнение шаблона из файла и url строки
              
               self::initRout($arguments[1],$matches[1]);//вытаскиваем из файла конроллер, метод или калбек и запускаем
      
           } 
        } elseif ($arguments[0]==$argumentMethod){ 
 
            self::initRout($arguments[1]);//вытаскиваем из файла конроллер, метод или калбек и запускаем
        
        }  


    }
   
   
}