<?php



namespace Mvccontainer\core\Routing;


interface RouterInterface
{
    public function route();
}