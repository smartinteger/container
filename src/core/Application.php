<?php
 
namespace Mvccontainer\core;
use Mvccontainer\core\Logging\{Logger,Format,FileWriter};
use Mvccontainer\core\Container\ServiceContainer;
class Application 
{
    /**
     *
     */
    public function run(): void
    {   
        
        
        $this->loadComponent(); 

      try {
       
          (new \Route(app()->get('file')['routes']))->route();
          
        }catch (\Exception $e){
            echo "исключение: {$e->getMessage()}<br/>";
            logwrite()->debug('Неперехваченное исключение Application Rout'.$e->getMessage());
              throw new \Exception('Routes file not found'); 
        }
        $logger = new Logger(new FileWriter, new Format);

    }

    /**     
     * автоматизирует создание object   
     * @return void
     */
    
    private function loadComponent(): void
    {
        // Component       
        foreach (app()->get('component') as $key => $volume) {
          
            app()->set($key, $volume)->createAlias($key, $key);
         
        }
        
        //Service
        foreach (app()->get('service') as $key => $volume) {
            app()->set($key, $volume['class'])->createAlias($key, $key); 
               
        }
        
    }

}