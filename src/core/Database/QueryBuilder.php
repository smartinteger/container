<?php


namespace Mvccontainer\core\Database;
use Mvccontainer\core\Application;

/**
 * Class QueryBuilder
 * @package Core\Database
 *
 * SELECT * FROM table
 */
class QueryBuilder implements QueryBuilderInterface
{
    protected $db;

    protected $table;
    protected $column = ['*'];
    protected $where = [];
    protected $limit = '';
    protected $offset = '';
    protected $order = [];

    protected $asArray;

    protected $modelClass;

    public function asArray()
    {
        $this->asArray = true;

        return $this;
    }

    public function setModel($modelClass)
    {
        $this->modelClass = $modelClass;

        return $this;
    }

    public function __construct()
    {
        $this->db = (new \Db())->createConcreteInstance();//DbFactory.php
    }

    public function select($columns): QueryBuilderInterface
    {
        $this->column = (array) $columns;

        return $this;
    }

    public function where($conditions): QueryBuilderInterface
    {
        $this->where = $conditions;

        return $this;
    }

    public function table($table): QueryBuilderInterface
    {
        $this->table = $table;

        return $this;
    }

    public function limit($limit): QueryBuilderInterface
    {
        $this->limit = $limit;

        return $this;
    }

    public function offset($offset): QueryBuilderInterface
    {
        $this->offset = $offset;

        return $this;
    }

    public function order($order): QueryBuilderInterface
    {
        $this->order = $order;

        return $this;
    }

    public function build(): string
    {
        if (empty($this->table)) {
            throw new \Exception('table is required');
        }

        $result = "SELECT " . implode(', ', $this->column);

        $result .= " FROM " . $this->table;

        if ($this->where) {
            $result .= " WHERE ";
            foreach ($this->where as $key => $value) {
                $where[] = $key . " = " . $value;
            }
            $result .= implode(" AND ", $where);
        }

        if ($this->order) {

            $result .= " ORDER BY ";

            foreach ($this->order as $key => $value) {
                $order[] = $key . " " . $value;
            }
            $result .= implode(", ", $order);
        }

        if ($this->limit) {
            $result .= " LIMIT " . $this->limit;

            if ($this->offset) {
                $result .= " OFFSET " . $this->offset;
            }
        }

        $this->reset();

        return $result;
    }

    public function one()
    {
        $this->limit = 1;
        $sql = $this->build();
        $response = $this->db->query($sql);
        $row = mysqli_fetch_assoc($response);

        if (
            !$this->asArray
            && $this->modelClass
            && is_a($this->modelClass, ActiveRecordInterface::class, true)
        ) {
            return $this->modelClass::createModel($row);
        }

        return $row;
    }

    public function all()
    {
        $sql = $this->build();
        $response = $this->db->query($sql);

        $rows = mysqli_fetch_all($response, MYSQLI_ASSOC);

        if (
            !$this->asArray
            && $this->modelClass
            && is_a($this->modelClass, ActiveRecordInterface::class, true)
        ) {
            return $this->modelClass::createModels($rows);
        }

        return $rows;
    }

    public function reset()
    {
        $this->table = null;
        $this->column = ['*'];
        $this->where = [];
        $this->limit = '';
        $this->offset = '';
        $this->order = [];
    }
}