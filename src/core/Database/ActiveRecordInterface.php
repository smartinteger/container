<?php


namespace Mvccontainer\core\Database;


interface ActiveRecordInterface
{
    public function delete();

    public function save();

    public function update();

    public function create();

    public static function createModels($rows);

    public static function createModel($attributes);

    public static function read($id);

    public static function find($condition = []);
}