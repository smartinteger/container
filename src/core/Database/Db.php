<?php


namespace Mvccontainer\core\Database;


use Mvccontainer\core\Contracts\ConfigurableInterface;


class Db implements ConfigurableInterface
{
    protected $config;

    protected $connection;

    protected $conn;

    protected static $instance;

    protected function __construct() {}

    protected function __clone() {}

    public function __destruct()
    {
        //$this->connection()->close();
    }

    static public function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function configure(array $config)
    {
       $this->config = $config;   
    }

    public function connect() {

        if (is_null($this->conn)) {
            $db = $this->config;
         
            $this->conn = mysqli_connect($db['host'], $db['user'], $db['pass'],$db['db']);

            if(!$this->conn) {
                echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
                echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
                echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
                exit;
            }
                       
            echo "Соединение с MySQL установлено!" . PHP_EOL;
            echo "Информация о сервере: " . mysqli_get_host_info($this->conn) . PHP_EOL;
        }
        return $this->conn;
    }

  

   
}