<?php


namespace Mvccontainer\core\Database;

use  Mvccontainer\core\Database\Db;

class DbFactory 
{

    public function createConcreteInstance()
    {
        Db::getInstance()->configure(config('Db')['connect']);
      
        return Db::getInstance()->connect();
    }
}

