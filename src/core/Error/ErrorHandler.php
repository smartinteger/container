<?php

namespace Mvccontainer\core\Error;

/**
 * Description of Error
 * Класс для обработки ошибок
 * set_exception_handler — Задает пользовательский обработчик исключений
 * @author Alex
 */

class ErrorHandler extends \Exception
{
    /**   
    * set_exception_handler — Задает пользовательский обработчик исключений
    * https://www.php.net/manual/ru/function.set-exception-handler.php
    */
    public function exceptionHandler(\Throwable  $e)
    {
        logwrite()->debug('Неперехваченное исключение'.$e->getMessage());
    }
}