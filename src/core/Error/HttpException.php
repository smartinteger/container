<?php

namespace Mvccontainer\core\Error;

class HttpException extends \Exception
{
    /**
     * List of additional headers
     *
     * @var array
     */

    private $headers;

 

    /**
     * List of HTTP status codes
     *
     * From http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
     *
     * @var array
     */
    private $error_code = array(
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',      
      
    );

    /**
     * @param int[optional]    $statusCode   Если NULL будет использовать 500 по умолчанию
     * @param string[optional] $message Если NULL будет использовать статусную фразу по умолчанию
     * @param array[optional]  $headers      List of additional headers
     */
    public function __construct($statusCode = 500, $message = null, array $headers = array())
    {
        if (null === $statusPhrase && isset($this->error_code[$statusCode])) {
            $message= $this->error_code[$statusCode];
        }
        parent::__construct($message, $statusCode);

        $header  = "HTTP/1.1 $statusCode $message";
    }


    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Returns the list of additional headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set response headers.
     *
     * @param array $headers Response headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }
    
   

    
}