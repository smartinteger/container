<?php


namespace Mvccontainer\core\Contracts;


interface BootstrapInterface
{
    function bootstrap();
}