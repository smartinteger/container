<?php


namespace Mvccontainer\core\Contracts;


interface ConfigurableInterface
{
    public function configure(array $config);
}