<?php


namespace Mvccontainer\core\Logging;
use DateTime;
error_reporting(E_ALL); ini_set('display_errors',1);
class DbWriter implements WriterInterface
{
    /**
     * @var PDO Подключение к БД
     */
    private $connection;

    public function __construct($attributes)
    {
       
        $this->connection = new \PDO($attributes['dsn'], $attributes['username'], $attributes['password']);
         
    }


    public function write($data, $attributes=[])
    {    
        $statement = $this->connection->prepare(
            'INSERT INTO log (level, message, context) VALUES (:level, :message, :context)'
        );
        $statement->bindParam(':level', $attributes['level']);
        $statement->bindParam(':message', $attributes['message']);
        $statement->bindParam(':context', $data);        
        $statement->execute();

       
    } 

    public function getLogDb(){

        $sql = 'SELECT * FROM log ';
        echo 'TXT LOG TABLE';
        foreach ($this->connection->query($sql) as $row) {
            echo  $row['id'] .' '.$row['date'] .' '.$row['level'] .' '.$row['message'] .' '.'<br>';
        }   
    }

    

}