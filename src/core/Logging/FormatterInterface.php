<?php


namespace Mvccontainer\core\Logging;


interface FormatterInterface
{
    public function format($level, $message, $context = []);
}