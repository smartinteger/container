<?php


namespace Mvccontainer\core\Logging;


class LoggerFactory 
{

    public function logText()
    {
        $writer = new FileWriter();
        $formatter = new Format();
        return new Logger($writer, $formatter);
    }
}