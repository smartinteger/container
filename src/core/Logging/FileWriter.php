<?php


namespace Mvccontainer\core\Logging;


class FileWriter implements WriterInterface
{

    public function write($data, $attributes=[])
    {
        $attributes['file']='/storage/logs/app.log';
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $attributes['file'], $data . PHP_EOL, FILE_APPEND);
    }
}


