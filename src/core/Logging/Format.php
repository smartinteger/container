<?php

namespace Mvccontainer\core\Logging;

use Mvccontainer\core\Logging\FormatterInterface;
use DateTime;
/**
 * Class Logger, расширяет 
 * абстрактный класс AbstractLogger и реализует интерфейс LoggerInterface.
 */

class Format implements FormatterInterface 
{
    
    /**
     * @var string Шаблон сообщения
     */
    public $template = '{date} {level} {message} {context}';


    /**
    * @var string Формат даты логов
     */
    public $dateFormat = DateTime::RFC2822;
    /**
     * Текущая дата
     *
     * @return string
     */
    public function getDate()
    {
        return (new DateTime())->format($this->dateFormat);
    }

    /**
     * Преобразование $context в строку
     *
     * @param array $context
     * @return string
     */


    public function contextStringify(array $context = [])
    {
        return !empty($context) ? json_encode($context) : null;
    }

    public function format($level, $message, $context = []) {
        return  trim(strtr($this->template, [
           '{date}' => $this->getDate(),
           '{level}' => $level,
           '{message}' => $message,
           '{context}' => $this->contextStringify($context),
        ])); 
        
    }
   

}