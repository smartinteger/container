<?php


namespace Mvccontainer\core\Logging;


interface WriterInterface
{
    public function write($data);
}