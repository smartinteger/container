<?php

namespace Mvccontainer\core\Logging;

use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

/**
 * Class Logger, расширяет 
 * абстрактный класс AbstractLogger и реализует интерфейс LoggerInterface.
 */

class Logger extends AbstractLogger implements LoggerInterface
{
    protected $writer;

    protected $formatter;
   

    public function __construct(WriterInterface $writer, FormatterInterface $formatter)
    {
        $this->writer = $writer;
        $this->formatter = $formatter;       
    }
    /**
     * @inheritdoc
     */

    /**
     * @inheritDoc
     */
    public function log($level, $message, $context = [])
    {
        $attributes['level']=$level;
        $attributes['message']=$message;
        $attributes['context']=$context;

        $data = $this->formatter->format($level, $message, $context);
        $this->writer->write($data, $attributes);
    }

    
    

}
