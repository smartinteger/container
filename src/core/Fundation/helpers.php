<?php

/**
* все доступные методы будут доступны всем классам
*/

/**
*app() возращает обьект сервис контейнера
*/

function app()
{
    //получаем объект ServiceContainer через метод получаем доступ ко всем методам Контейнера
    return \Mvccontainer\core\Container\ServiceContainer::getInstance();
}

/*запись в файл логов*/
function logwrite(){
   return (new \logger)->logText();
}

function config($key){
    return app()->get('service')[$key]['depend'];
 }
