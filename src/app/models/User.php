<?php


namespace Mvccontainer\App\Models;

use Mvccontainer\core\Database\Model;
use DateTime;

class User extends Model
{
  /**
    * @var string Формат даты 
     */
    public $dateFormat = DateTime::RFC2822;

    public function getDate()
    {
        return (new DateTime())->format('Y-m-d H:i:s');
    }

    protected static function tableName()
    {
        return 'users';
    }

    public function save()
    {
        
        if ($this->exists()) {
            return $this->update();
        } else {
            return $this->create();
        }
    }
    public function create()
    {
        $this->password = crypt($this->password, 'solim');
        $this->created_at = $this->getDate();
        parent::create();
    }
    public function update()
    {
        
        $this->password = crypt($this->password, 'solim');
       

        $this->updated_at = $this->getDate();
        parent::update();
    }






}