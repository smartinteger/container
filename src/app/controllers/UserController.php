<?php

namespace Mvccontainer\App\Controllers;
use Mvccontainer\App\Models\User;

class UserController
{
   
    public function IndexAction($id=null){
        
      // Получение одиночной модели из базы данных
      $id= intval($id);

      $post = User::find()->where(['id' => $id])->one();
      print_r($post);
      
      $posts = User::find()->all();
      print_r($posts);
    }

    public function readAction($id=null){
        
        // Получение одиночной модели из базы данных
        $id = intval($id);
   
            $post = User::find()->where(['id' => $id])->one();
            print_r($post);
      
            $posts = User::find()->all();
            print_r($posts);
      
      }
      public function readAllAction(){
        
      
      
            $posts = User::find()->all();
            print_r($posts);
      
      }

    public function CreateAction($id=null){
         // Создание новой модели и сохранение ее в базу данных
         $user_model = new User();
         //first_name=Ivan&last_name=Ivanov&email=ivanov.ivan@gmail.com&password=1q2w3e
         $first_name = $_GET['first_name'];
         $last_name = $_GET['last_name'];
         $email = $_GET['email'];
         $user_model->first_name =  $first_name;
         $user_model->last_name = $last_name;
         $user_model->email = $email;
         $user_model->password = $_GET['password'];
      
         if ($user_model->save()) {
             echo 'Модель успешно создана' . PHP_EOL;
         }
         print_r($user_model);
      }

    public function UpdateAction($id=null){
             // Создание новой модели и сохранение ее в базу 
           //http://mvc-container/user/update?id=5&first_name=Ivanor&last_name=Ivano&email=ivanov.ivan@gmail.com&password=1q2w3e
             $user_model = User::find()->where(['id' =>$_GET['id']])->one();
            
             //first_name=Ivan&last_name=Ivanov&email=ivanov.ivan@gmail.com&password=1q2w3e
             $user_model->first_name = $_GET['first_name'];
             $user_model->last_name = $_GET['last_name'];
             $user_model->email = $_GET['email'];
             $user_model->password = $_GET['password'];
          
             if ($user_model->save()) {
                 echo 'Модель успешно обновлена' . PHP_EOL;
             }
             print_r($user_model);
       
    }

    public function DeleteAction($id){
       
        $user_model = User::find()->where(['id' =>$id])->one();
        // Удаление модели из базы данных
        if ($user_model->delete()) {
            echo 'Модель успешно удалена' . PHP_EOL;
        }
    }

  

}