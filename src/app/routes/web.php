<?php

Route::get('/', function() {
    echo 'Моя Главная страница';
});

Route::get('/index/index', 'IndexController@Index');

Route::get('/index/index/{integer}', 'IndexController@IndexId');

Route::get('/user/index', 'UserController@Index');
Route::get('/user/read/{integer}', 'UserController@Read');
Route::get('/user/read', 'UserController@ReadAll');
Route::get('/user/create', 'UserController@Create');

Route::get('/user/update', 'UserController@Update');
Route::get('/user/delete/{integer}', 'UserController@Delete');