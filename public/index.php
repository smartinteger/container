<?php
require '../vendor/autoload.php';
use Mvccontainer\core\Logging\{Logger,Format,FileWriter};

$logger = new Logger(new \Mvccontainer\core\Logging\FileWriter, new Format);
$logger->critical("Critical message");
(new \Mvccontainer\Core\Application)->run();